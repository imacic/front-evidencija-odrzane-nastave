import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../service/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private toastr: ToastrService, private userService: UserService) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean  {
      if(localStorage.getItem('user') != null) {
        let role = next.data["role"] as String;
        if(role) {
          let match = this.userService.roleMatch(role);
          if(match) return true;
          else {
            this.router.navigate(['/dashboard/home']);
            this.toastr.warning("Nemate dozvolu za pristupanje ovog mrežnog resursa", "Upozorenje!");
            return false;
          }
        }
        else {
          return true;
        }
      }
      else {
        this.router.navigate(['/user/login']);
        this.toastr.warning("Morate se prijaviti da biste pristupili mrežnom resursu", "Upozorenje!");
        return false;
      }
  }
  
}
