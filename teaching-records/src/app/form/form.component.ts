import { Component, OnInit } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { RecordsService } from '../service/records.service';
import { Subject } from '../models/Subject';
import { Classroom } from '../models/Classroom';
import { parseDate } from 'ngx-bootstrap/chronos/public_api';
import { UserService } from '../service/user.service';
import { Department } from '../models/Department';
import { Lecturer } from '../models/Lecturer';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  user = JSON.parse(localStorage.getItem("user"));
  isChecked: Boolean = true;
  departmentId: Number;
  selectLecturer: Boolean = false;
  isEmptyLecturer: Boolean = true;
  isEmptySubject: Boolean = false;

  allDepartments: Array<Department> = [];
  allLecturersByDepId: Array<Lecturer> = [];
  allSubjectsbyDepId: Array<Subject> = [];
  allClassrooms: Array<Classroom> = [];

  formModel = {
    lecturerID: this.user.id,
    subjectID: 0,
    classroomID: 0,
    numberOfClasses: 0,
    type: 'Predavanja',
    date: new Date(),
    time: new Date(),
    maintained: '',
    comment: ''
  }

  resetedFormModel = {
    lecturerID: this.user.id,
    subjectID: 0,
    classroomID: 0,
    numberOfClasses: 0,
    type: 'Predavanja',
    date: new Date(),
    time: new Date(),
    maintained: true,
    comment: ''
  }

  constructor(private service: RecordsService, private toastr: ToastrService, public userService: UserService) { }

  ngOnInit(): void {
    this.setCurrentUser();
    this.getClassrooms();
    //this.getLecturersByDepId();
    this.getDepartments();
    //console.log(this.user);
    

    
  }

  numOfClassesDefault() {
    if(this.isChecked == true) {
      this.formModel.numberOfClasses = 4;
    }
    else {
      this.formModel.numberOfClasses = 0;
    }
  }

  formatDate() {
    let now = this.formModel.date;
    let month = now.getMonth()+1;
    let date = now.getDate();
    let year = now.getFullYear();
    let parsedDate, parsedMonth, parsedYear;
    if(month < 10) {
      parsedMonth = "0" + month.toString(); 
    }
    else {
      parsedMonth = month.toString();
    } 

    if(date < 10) {
      parsedDate = "0" + date.toString(); 
    }
    else {
      parsedDate = date.toString();
    } 

    parsedYear = year.toString();

    let currentDate = parsedMonth + "/" + parsedDate + "/" + parsedYear;
    return currentDate;
  }
  
  formatTime() {
    let now = this.formModel.time;
    let hours = now.getHours();
    let minutes = now.getMinutes();
    let parsedHours, parsedMinutes;
    if(hours < 10) {
      parsedHours = "0" + hours.toString();
    }
    else {
      parsedHours = hours.toString();
    }

    if(minutes < 10) {
      parsedMinutes = "0" + minutes.toString();
    }
    else {
      parsedMinutes = minutes.toString();
    }

    let currentTime = parsedHours+":"+parsedMinutes;
    return currentTime;
  }

  onSubmit(form: NgForm) {
    form.value.lecturerID = Number(form.value.lecturerID);
    form.value.subjectID = Number(form.value.subjectID);
    form.value.classroomID = Number(form.value.classroomID);
    form.value.numberOfClasses? form.value.numberOfClasses = Number(form.value.numberOfClasses) : null;
    form.value.time = this.formatTime();
    form.value.date = this.formatDate();
    //form.value.maintained? form.value.comment = '' : form.value.comment;
    console.log(form.value);

    this.service.record(form.value).subscribe(
      (res: any) => {
        this.toastr.success("Uspešno ste uneli!");
        form.resetForm(this.resetedFormModel);
        if(this.userService.roleMatch('admin')) {
          this.departmentId = 0;
          this.selectLecturer = false;
          this.isEmptySubject = false;
        }
        
      },
      err => {
        this.toastr.error("Neuspešan unos!");
        console.log(err);
      }
    )
  }

  getSubjectsbyDepId() {
    this.service.getSubjectsByDepId(this.departmentId).subscribe(
      (res: any) => {
        if(res.length > 0) {
          this.allSubjectsbyDepId = res;
          this.isEmptySubject = true;
          this.formModel.subjectID = 0;
        }
        else {
          this.isEmptySubject = false;
          this.formModel.subjectID = 1234567890;
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  getClassrooms() {
    this.service.getClassrooms().subscribe(
      (res: any) => {
        this.allClassrooms = res;
      },
      err => {
        console.log(err);
      }
    )
  }

  getLecturersByDepId() {
    this.service.getLecturersByDepId(this.departmentId).subscribe(
      (res: any) => {
        if(res.length > 0) {
          this.allLecturersByDepId = res;
          this.selectLecturer = true;
          this.formModel.lecturerID = 0;
          this.isEmptyLecturer = true;
        }
        else {
          this.selectLecturer = false;
          this.isEmptyLecturer = false;
          this.formModel.lecturerID = "noLecturer";
        }
        
      },
      err => {
        console.log(err);
      }
    )
  }

  getDepartments() {
    this.service.getDepartments().subscribe(
      (res: any) => {
        this.allDepartments = res;
      },
      err => {
        console.log(err);
      }
    )
  }

  setCurrentUser() {
    if(this.userService.roleMatch('admin')) {
      this.formModel.lecturerID = 0;
      this.resetedFormModel.lecturerID = 0;
      this.resetedFormModel.subjectID = 0;
      this.departmentId = 0;
    }
    else {
      this.departmentId = this.user.departmentID;
      this.formModel.lecturerID = this.user.id;
      this.getSubjectsbyDepId();
    }
  }

}
