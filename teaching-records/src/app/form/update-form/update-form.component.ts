import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RecordsService } from 'src/app/service/records.service';
import { Subject } from 'src/app/models/Subject';
import { Classroom } from 'src/app/models/Classroom';
import { Lecturer } from 'src/app/models/Lecturer';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { TeachingRecordUpdate } from 'src/app/models/TeachingRecordUpdate';
import { Department } from 'src/app/models/Department';
import { UserService } from 'src/app/service/user.service';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
  record: TeachingRecordUpdate;
  recordId: Number;

  user = JSON.parse(localStorage.getItem("user"));
  isChecked: Boolean = undefined;
  departmentId: Number;
  selectLecturer: Boolean = false;
  isEmptyLecturer: Boolean = true;
  isEmptySubject: Boolean = false;

  allDepartments: Array<Department> = [];
  allLecturersByDepId: Array<Lecturer> = [];
  allSubjectsbyDepId: Array<Subject> = [];
  allClassrooms: Array<Classroom> = [];

  formModel = {
    id: undefined,
    lecturerID: undefined,
    subjectID: undefined,
    classroomID: undefined,
    numberOfClasses: undefined,
    type: undefined,
    date: undefined,
    time: undefined,
    comment: undefined
  }

  constructor(private route: ActivatedRoute, private service: RecordsService, private toastr: ToastrService, private router: Router, public userService: UserService) { }

  ngOnInit(): void {
    this.route.paramMap
      .subscribe(params => {
        this.recordId  =+ params.get('id');
      });

    this.getRecordById();  
    this.getDepartments();
    this.getClassrooms();
  }

  getRecordById() {
    this.service.getRecordById(this.recordId).subscribe(
      (res: any) => {
        this.record = res;
        //console.log(this.record);

        this.departmentId = this.record.departmentID;

        this.getLecturers();
        this.getSubjects();

        this.formModel.id = this.recordId;
        this.formModel.lecturerID = this.record.lecturerID;
        this.formModel.subjectID = this.record.subjectID;
        this.formModel.classroomID = this.record.classroomID;
        this.record.numberOfClasses != 0 ? this.formModel.numberOfClasses = this.record.numberOfClasses : this.formModel.numberOfClasses = 4;
        this.formModel.type = this.record.type;
        this.formModel.date = new Date(String(this.record.date));
        this.formModel.time = new Date("2020-01-01T"+this.record.time);
        this.isChecked =  this.record.maintained;
        this.formModel.comment = this.record.comment;

      },
      err => {
        console.log(err);
      }
    )
  }

  formatDate() {
    let now = this.formModel.date;
    let month = now.getMonth()+1;
    let date = now.getDate();
    let year = now.getFullYear();
    let parsedDate, parsedMonth, parsedYear;
    if(month < 10) {
      parsedMonth = "0" + month.toString(); 
    }
    else {
      parsedMonth = month.toString();
    } 

    if(date < 10) {
      parsedDate = "0" + date.toString(); 
    }
    else {
      parsedDate = date.toString();
    } 

    parsedYear = year.toString();

    let currentDate = parsedMonth + "/" + parsedDate + "/" + parsedYear;
    //console.log(currentDate);
    return currentDate;
  }
  
  formatTime() {
    let now = this.formModel.time;
    let hours = now.getHours();
    let minutes = now.getMinutes();
    let parsedHours, parsedMinutes;
    if(hours < 10) {
      parsedHours = "0" + hours.toString();
    }
    else {
      parsedHours = hours.toString();
    }

    if(minutes < 10) {
      parsedMinutes = "0" + minutes.toString();
    }
    else {
      parsedMinutes = minutes.toString();
    }

    let currentTime = parsedHours+":"+parsedMinutes;
    return currentTime;
  }

  onSubmit(form: NgForm) {
    form.value.subjectID = Number(form.value.subjectID);
    form.value.classroomID = Number(form.value.classroomID);
    form.value.lecturerID = Number(form.value.lecturerID);
    form.value.numberOfClasses? form.value.numberOfClasses = Number(form.value.numberOfClasses) : null;
    //form.value.maintained? form.value.comment = '' : form.value.comment;
    form.value.time = this.formatTime();
    form.value.date = this.formatDate();
    //console.log(form.value);

    this.service.update(form.value).subscribe(
      (res: any) => {
        this.toastr.success("Uspešno ste izmenili evidenciju!");
        this.router.navigateByUrl('/dashboard/records');
      },
      err => {
        this.toastr.error("Neuspešan unos!");
        console.log(err);
      }
    )
  }

  numOfClassesDefault() {
    if(this.isChecked == true) {
      this.formModel.numberOfClasses = 4;
    }
    else {
      this.formModel.numberOfClasses = 0;
    }
  }
  getDepartments() {
    this.service.getDepartments().subscribe(
      (res: any) => {
        this.allDepartments = res;
      },
      err => {
        console.log(err);
      }
    )
  }
  
  getSubjects() {
    this.service.getSubjectsByDepId(this.departmentId).subscribe(
      (res: any) => {
        if(res.length > 0) {
          this.allSubjectsbyDepId = res;
          this.isEmptySubject = true;
        }
        else {
          this.isEmptySubject = false;
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  getSubjectsbyDepId() {
    this.service.getSubjectsByDepId(this.departmentId).subscribe(
      (res: any) => {
        if(res.length > 0) {
          this.allSubjectsbyDepId = res;
          this.isEmptySubject = true;
          if(this.userService.roleMatch('admin')) {
            this.formModel.subjectID = 0;
          }
          
        }
        else {
          this.isEmptySubject = false;
          this.formModel.subjectID = 1234567890;
        }
      },
      err => {
        console.log(err);
      }
    )
  }

  getClassrooms() {
    this.service.getClassrooms().subscribe(
      (res: any) => {
        this.allClassrooms = res;
      },
      err => {
        console.log(err);
      }
    )
  }

  getLecturers() {
    this.service.getLecturersByDepId(this.departmentId).subscribe(
      (res: any) => {
        if(res.length > 0) {
          this.allLecturersByDepId = res;
          this.selectLecturer = true;
          this.isEmptyLecturer = true;
        }
        else {
          this.selectLecturer = false;
          this.isEmptyLecturer = false;
        }
        
      },
      err => {
        console.log(err);
      }
    )
  }

  getLecturersByDepId() {
    this.service.getLecturersByDepId(this.departmentId).subscribe(
      (res: any) => {
        if(res.length > 0) {
          this.allLecturersByDepId = res;
          this.selectLecturer = true;
          this.formModel.lecturerID = 0;
          this.isEmptyLecturer = true;
        }
        else {
          this.selectLecturer = false;
          this.isEmptyLecturer = false;
          this.formModel.lecturerID = "noLecturer";
        }
        
      },
      err => {
        console.log(err);
      }
    )
  }

}
