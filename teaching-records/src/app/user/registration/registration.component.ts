import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Department } from 'src/app/models/Department';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  allDepartments: Array<Department> = [];

  constructor(public service: UserService, private router: Router, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.service.formModel.reset();
    this.getDepartments();

  }

  onSubmit() {
    this.service.register().subscribe(
      (res:any) => {
        if(res == true) {
          this.service.formModel.reset();
          this.toastr.success('Novi korisnik kreiran !', 'Registracija uspesna !' );
        }
      },
      err => {
        console.log(err);
        if(err.status == 400) {
          this.toastr.error("Korisnicko ime je vec zauzeto!", 'Registracija neuspesna !');
        }
        else {
          this.toastr.error("Registracija neuspesna !");
        }
      }
    );
  }

  getDepartments() {
    this.service.getDepartments().subscribe(
      (res: any) => {
        this.allDepartments = res;
      },
      err => {
        console.log(err);
      }
    )
  }

}
