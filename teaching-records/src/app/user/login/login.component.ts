import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/service/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Lecturer } from 'src/app/models/Lecturer';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formModel = {
    UserName: '',
    Password: ''
  }

  constructor(private service: UserService, private router: Router, private toastr: ToastrService) { 
    if(localStorage.getItem('user') != null) {
      this.router.navigateByUrl('/dashboard/home');
    }
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    this.service.login(form.value).subscribe(
      (res: any) => {
        this.router.navigateByUrl("/dashboard/home");
        this.toastr.success("Uspešno ste prijavljeni !" , "Dobro došli");
        localStorage.setItem("user", JSON.stringify(res));
        localStorage.setItem("role", JSON.stringify(res.status));
      },
      err => {
        console.log(err);
        this.toastr.error("Pogrešno korisničko ime ili lozinka !","Neuspešna prijava");
      }
    )
  }

  

}
