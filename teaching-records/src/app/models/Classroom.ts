export class Classroom {
    id: String;
    number: String;
    type: String;
    capacity: String;
    floor: String;
    fullHDProjector: String;
}