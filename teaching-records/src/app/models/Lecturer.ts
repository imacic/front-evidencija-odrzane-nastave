export class Lecturer {
    id: Number;
    departmentID: Number;
    name: String;
    surname: String;
    username: String;
    password: String;
    status: String;
}