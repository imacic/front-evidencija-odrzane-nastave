export class TeachingRecord {
    id: String;
    date: String;
    time: String;
    type: String;
    maintained: String;
    numberOfClasses: String;
    subjectName: String;
    lecturerID: String;
    lecturerFullname: String;
    departmentName: String;
    classroomNumber: String;
    comment: String;
}