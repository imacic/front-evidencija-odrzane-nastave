export class TeachingRecordUpdate {
    id: Number;
    date: String;
    time: String;
    type: String;
    maintained: Boolean;
    numberOfClasses: Number;
    subjectID: Number;
    lecturerID: Number;
    lecturerName: String;
    lecturerSurname: String;
    departmentID: Number;
    departmentName: String;
    classroomID: Number;
    comment: String;
}