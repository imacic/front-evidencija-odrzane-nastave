export class LecturerJoin {
    id: Number;
    departmentID: Number;
    fullName: String;
    username: String;
    password: String;
    status: String;
}