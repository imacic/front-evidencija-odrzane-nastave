import { Component, OnInit } from '@angular/core';
import { RecordsService } from '../service/records.service';
import { UserService } from '../service/user.service';
import { Lecturer } from '../models/Lecturer';
import * as html2pdf from 'html2pdf.js'
import * as $ from 'jquery' 

import pdfMake from "bower_components/pdfmake/build/pdfmake";
import pdfFonts from "bower_components/pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from "html-to-pdfmake"


import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import { TeachingRecord } from '../models/TeachingRecord';
import { element } from 'protractor';
import { LecturerJoin } from '../models/LecturerJoin';
import {imageLogo } from './logo'

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  export() {
    var element = document.getElementById("export");3
    var heading = htmlToPdfmake("<h2>Izveštaj svih predavača</h2>");
    var content = htmlToPdfmake(element.innerHTML);

    var docDefinition = {
      info: {
        title: 'Izveštaj svih predavača',
      },
      pageSize: 'A4',

      pageOrientation: 'landscape',

      pageMargins: [ 100, 20, 100, 10 ],
        content: [
          {
            text: heading,
            alignment: 'center'

          },
          {
            image: imageLogo,
            width: 60,
            alignment: 'right',
            margin: [0,50,10,-60]
        },
          content

        ],
      };

      const pdfDocGenerator = pdfMake.createPdf(docDefinition).download("Izveštaj svih predavača");
  }



/*

  exportAsConfig: ExportAsConfig = {
    type: 'pdf', // the type you want to download
    elementIdOrContent: 'export', // the id of html/table element
  }

  exportAsConfigLecturer: ExportAsConfig = {
    type: 'pdf', // the type you want to download
    elementIdOrContent: 'exportLecturer', // the id of html/table element
  }
  

  export() {
    this.exportAsService.save(this.exportAsConfig, 'Izvestaj_svih_predavaca').subscribe(() => {
    });
  }

  exportLecturer() {
    this.exportAsService.save(this.exportAsConfigLecturer, this.reportName).subscribe(() => {
    });
  }

*/
  /*

  onExportClick() {
    const options = {
      filename: 'Izvestaj_svih_predavaca',
      html2canvas: {},
      jsPDF: { orientation: 'portrait'}
    }

    const content: Element = document.getElementById('export');

    html2pdf()
      .from(content)
      .set(options)
      .toPdf()
      .save();
  };

  */
  

  user = JSON.parse(localStorage.getItem("user"));
  allLecturers: Array<LecturerJoin> = [];
  allRecordsByLecturerId: Array<TeachingRecord> = [];
  searchLecturer: Number = 0;
  reportName: string;
  isLoaded: Boolean = false;

  chartArray: any[] = [
    {
      name: "Održani",
      value: 0
    },
    {
      name: "Neodržani",
      value: 0
    },
    {
      name: "Održani (M)",
      value: 0
    },
    {
      name: "Neodržani (M)",
      value: 0
    }
  ]

  maintainedClasses: Number = 0;
  notMaintainedClasses: Number = 0;

  maintainedClassesMonth: Number = 0;
  notMaintainedClassesMonth: Number = 0;

  maintainedClassesLecturer: Number = 0;
  notMaintainedClassesLecturer: Number = 0;

  maintainedClassesMonthLecturer: Number = 0;
  notMaintainedClassesMonthLecturer: Number = 0;

  constructor(private service: RecordsService, public userService: UserService, private exportAsService: ExportAsService) { }

  ngOnInit(): void {
    if(this.userService.roleMatch('admin')) {
      this.allRecords();
      this.getLecturers();
    }
    else {
      this.allRecordsById();
    }
    
  }


  allRecords() {
    this.service.getAllRecords().subscribe(
      (res: any) => {
        let counter = 0;
        res.forEach(element => {
          if(element.maintained) {
            this.maintainedClasses += element.numberOfClasses;
          }
          else {
            counter++;
          }
        });
        this.notMaintainedClasses = counter;

        this.chartArray[0].value = this.maintainedClasses;
        this.chartArray[1].value = this.notMaintainedClasses;

        counter = 0;
        res.forEach(element => {
          if(element.maintained && element.date > this.getMonth() + "/31/2020") {
            this.maintainedClassesMonth += element.numberOfClasses;
          }
          else if(!element.maintained && element.date > this.getMonth() + "/31/2020") {
            counter++;
          }
        });
        this.notMaintainedClassesMonth = counter;
        

        this.chartArray[2].value = this.maintainedClassesMonth;
        this.chartArray[3].value = this.notMaintainedClassesMonth;
        this.isLoaded = true;

      },
      err => {
        console.log(err);
      }
    )
  }

  allRecordsById(){
    let user = JSON.parse(localStorage.getItem("user"));
    this.service.getRecords(user).subscribe(
      (res: any) => {

        let counter = 0;
        res.forEach(element => {
          if(element.maintained) {
            this.maintainedClasses += element.numberOfClasses;
          }
          else {
            counter++;
          }
        });
        this.notMaintainedClasses = counter;


        counter = 0;
        res.forEach(element => {
          if(element.maintained && element.date > this.getMonth() + "/30/2020") {
            this.maintainedClassesMonth += element.numberOfClasses;
          }
          else if(!element.maintained && element.date > this.getMonth() + "/30/2020") {
            counter++;
          }
        });
        this.notMaintainedClassesMonth = counter;
      },
      err => {
        console.log(err);
      }
    )
  }

  getMonth() {
    let now = new Date();
    let month = now.getMonth();
    
    let parsedMonth;
    if(month < 10) {
      parsedMonth = "0" + month.toString(); 
    }
    else {
      parsedMonth = month.toString();
    }
    return parsedMonth;
  }

  getLecturers() {
    this.service.getLecturersJoin().subscribe(
      (res: any) => {
        this.allLecturers = res;
      },
      err => {
        console.log(err);
      }
    )
  }
  
  getRecordsByLecturerId() {
    this.maintainedClassesLecturer = 0;
    this.notMaintainedClassesLecturer = 0;

    this.maintainedClassesMonthLecturer = 0;
    this.notMaintainedClassesMonthLecturer = 0;


    this.service.getRecordsByLecturerId(this.searchLecturer).subscribe(
      (res: any) => {
        let counter = 0;
        res.forEach(element => {
          if(element.maintained) {
            this.maintainedClassesLecturer += element.numberOfClasses;
          }
          else {
            counter++;
          }
        });
        if(res.length > 0) {
          this.reportName = "Izvestaj_"+ res[0].lecturerFullname;
        }
        this.notMaintainedClassesLecturer = counter;

        counter = 0;
        res.forEach(element => {
          if(element.maintained && element.date > this.getMonth() + "/30/2020") {
            this.maintainedClassesMonthLecturer += element.numberOfClasses;
          }
          else if(!element.maintained && element.date > this.getMonth() + "/30/2020") {
            counter++;
          }
        });
        this.notMaintainedClassesMonthLecturer = counter;

      },
      err => {
        console.log(err);
      }
    )
  }

}
