import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OrderModule } from 'ngx-order-pipe';
import { ExportAsModule } from 'ngx-export-as';
//import { SelectDropDownModule } from 'ngx-select-dropdown'
import {NgSelectModule, NgOption} from '@ng-select/ng-select';
import { NgxChartsModule } from '@swimlane/ngx-charts';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TemplateComponent } from './template/template.component';
import { HeaderComponent } from './template/header/header.component';
import { SidebarComponent } from './template/sidebar/sidebar.component';
import { FooterComponent } from './template/footer/footer.component';
import { FormComponent } from './form/form.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserService } from './service/user.service';
import { HttpClientModule } from '@angular/common/http';
import { RecordsComponent } from './records/records.component';
import { ErrorComponent } from './error/error.component';
import { RecordsFilterPipe } from './records/records-filter.pipe';
import { ReportComponent } from './report/report.component';
import { UpdateFormComponent } from './form/update-form/update-form.component';

@NgModule({
  declarations: [
    AppComponent,
    TemplateComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    FormComponent,
    UserComponent,
    LoginComponent,
    RegistrationComponent,
    RecordsComponent,
    ErrorComponent,
    RecordsFilterPipe,
    ReportComponent,
    UpdateFormComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    PaginationModule.forRoot(),
    NgbModule,
    OrderModule,
    ExportAsModule,
    NgSelectModule,
    NgxChartsModule
    
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
