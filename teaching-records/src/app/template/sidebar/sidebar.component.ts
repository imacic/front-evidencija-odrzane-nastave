import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/service/user.service';


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  //role = JSON.parse(localStorage.getItem("role"));

  constructor(public userService: UserService) { }

  ngOnInit(): void {
  }

}
