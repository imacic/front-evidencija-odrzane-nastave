import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery' 
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  expand = false;
  
  user = JSON.parse(localStorage.getItem("user"));

  constructor(private router: Router) { }

  ngOnInit(): void {
    $(document).ready(function(){
      $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
        });
    });
  }

  toggle() {
    if (this.expand == false)  {
      this.expand = true;
    }
    else {
      this.expand = false;
    }
  }
  
  onLogout() {
    localStorage.removeItem('user');
    this.router.navigate(['/user/login']);
  }

  
}
