import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormComponent } from './form/form.component';
import { FooterComponent } from './template/footer/footer.component';
import { LoginComponent } from './user/login/login.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserComponent } from './user/user.component';
import { TemplateComponent } from './template/template.component';
import { RecordsComponent } from './records/records.component';
import { ErrorComponent } from './error/error.component';
import { AuthGuard } from './guards/auth.guard';
import { ReportComponent } from './report/report.component';
import { UpdateFormComponent } from './form/update-form/update-form.component';


const routes: Routes = [
  {
    path: '', 
    redirectTo: '/user/login',
    pathMatch: 'full'
  },
  {
    path: 'user', 
    redirectTo: '/user/login',
    pathMatch: 'full'
  },
  { 
    path: 'user', component: UserComponent,
    children: [
      { path: 'registration', component: RegistrationComponent, canActivate: [AuthGuard], data: { role: 'admin' }},
      { path: 'login', component: LoginComponent}
    ]
  },
  {
    path: 'dashboard', 
    redirectTo: '/dashboard/home',
    pathMatch: 'full'
  },
  { 
    path: 'dashboard', component: TemplateComponent, canActivate: [AuthGuard], 
    children: [
      { path: 'home', component: FormComponent},
      { path: 'records/:id', component: UpdateFormComponent},
      { path: 'records', component: RecordsComponent},
      { path: 'report', component: ReportComponent}
    ]
  },
  { 
    path: '**', component: TemplateComponent,
    children: [
      { path: '**', component: ErrorComponent}
      
    ]
  }

  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
