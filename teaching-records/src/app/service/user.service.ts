import { Injectable } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private fb:FormBuilder, private http:HttpClient) { }
  readonly BaseUrl = "https://localhost:44300/api";

  formModel = this.fb.group({
    Name : ['', Validators.required],
    Surname : ['', Validators.required],
    Username : ['', Validators.required],
    Passwords: this.fb.group({
      Password : ['', [Validators.required, Validators.minLength(8)]],
      ConfirmPassword : ['', Validators.required]
    }, { validator : this.comparePasswords }
    ),
    Department : ['', Validators.required],
    Status : ['', Validators.required]
  })

  comparePasswords(fg:FormGroup) {
    let confirmPswrdCtrl = fg.get('ConfirmPassword');
    if(confirmPswrdCtrl.errors == null || 'passwordMismatch' in confirmPswrdCtrl.errors) {
      if(fg.get('Password').value != confirmPswrdCtrl.value)
        confirmPswrdCtrl.setErrors({passwordMismatch:true});
      else
        confirmPswrdCtrl.setErrors(null);
    }
  }

  register() {
    var body = {
      Name: this.formModel.value.Name,
      Surname: this.formModel.value.Surname,
      Username: this.formModel.value.Username,
      Password: this.formModel.value.Passwords.Password,
      DepartmentID: Number(this.formModel.value.Department),
      Status: this.formModel.value.Status
    };
    return this.http.post(this.BaseUrl + '/Lecturer/insert', body);
  }

  login(formData) {
    return this.http.post(this.BaseUrl + '/Lecturer/login', formData);
  }

  roleMatch(allowedRole): boolean {
    let isMatch = false;
    let admin = JSON.parse(localStorage.getItem("role"));
    if(allowedRole == admin) {
      isMatch = true;
    }
    return isMatch;
  }

  getDepartments() {
    return this.http.get(this.BaseUrl + '/Department');
  }

}
