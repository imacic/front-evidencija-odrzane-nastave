import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecordsService {

  constructor(private http:HttpClient) { }

  readonly BaseUrl = "https://localhost:44300/api";

  getAllRecords(){
    return this.http.get(this.BaseUrl + '/TeachingRecords/join');
  }

  getRecords(user){
    return this.http.post(this.BaseUrl + '/TeachingRecords/records', user);
  }

  getRecordById(id){
    return this.http.get(this.BaseUrl + '/TeachingRecords/'+ id);
  }
  
  getRecordsByLecturerId(id){
    return this.http.get(this.BaseUrl + '/TeachingRecords/records/'+ id);
  }
  getSubjects() {
    return this.http.get(this.BaseUrl + '/Subject');
  }
  
  getSubjectsByDepId(id) {
    return this.http.get(this.BaseUrl + '/Subject/' + id);
  }

  getClassrooms() {
    return this.http.get(this.BaseUrl + '/Classroom');
  }

  getLecturers() {
    return this.http.get(this.BaseUrl + '/Lecturer');
  }
  getLecturersJoin() {
    return this.http.get(this.BaseUrl + '/Lecturer/join');
  }

  getLecturersByDepId(id) {
    return this.http.get(this.BaseUrl + '/Lecturer/' + id);
  }
  
  getDepartments() {
    return this.http.get(this.BaseUrl + '/Department');
  }

  record(formData) {
    return this.http.post(this.BaseUrl + '/TeachingRecords/insert', formData);
  }

  update(formData) {
    return this.http.put(this.BaseUrl + '/TeachingRecords/update', formData);
  }

  delete(id) {
    return this.http.delete(this.BaseUrl + '/TeachingRecords/'+ id + '/delete');
  }

}

