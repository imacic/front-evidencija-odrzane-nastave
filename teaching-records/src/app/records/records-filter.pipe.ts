import { Pipe, PipeTransform } from '@angular/core';
import { TeachingRecord } from '../models/TeachingRecord';
import { RecordsComponent } from './records.component';

@Pipe({
  name: 'recordsFilter'
})
export class RecordsFilterPipe implements PipeTransform {

  transform(records: TeachingRecord[], searchSubject: String, searchClassroom: String, searchNumber: String, searchType: String, searchLecturer: String, searchDepartment: String): TeachingRecord[] {
    
    if(!records || !searchSubject && !searchClassroom && !searchType && !searchNumber && !searchLecturer && !searchDepartment) {
      return records;
    }
    

    return records.filter(record =>  
      record.subjectName.toLowerCase().indexOf(searchSubject.toLowerCase()) !== -1 &&
      record.classroomNumber.toString().toLowerCase().indexOf(searchClassroom.toString()) !== -1 &&
      record.numberOfClasses.toString().toLowerCase().indexOf(searchNumber.toLowerCase()) !== -1 &&
      record.type.toLowerCase().indexOf(searchType.toLowerCase()) !== -1 &&
      record.lecturerFullname.toLowerCase().indexOf(searchLecturer.toLowerCase()) !== -1 &&
      record.departmentName.toLowerCase().indexOf(searchDepartment.toLowerCase()) !== -1
    )
  }

}
