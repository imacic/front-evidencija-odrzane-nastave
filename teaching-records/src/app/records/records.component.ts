import { Component, OnInit } from '@angular/core';
import { RecordsService } from '../service/records.service';
import { TeachingRecord } from '../models/TeachingRecord';
import { PageChangedEvent } from 'ngx-bootstrap/pagination/public_api';
import * as $ from 'jquery' 
import { Subject } from '../models/Subject';
import { Classroom } from '../models/Classroom';
import { OrderPipe } from 'ngx-order-pipe';
import { UserService } from '../service/user.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Lecturer } from '../models/Lecturer';
import { Department } from '../models/Department';
import { LecturerJoin } from '../models/LecturerJoin';

@Component({
  selector: 'app-records',
  templateUrl: './records.component.html',
  styleUrls: ['./records.component.css']
})
export class RecordsComponent implements OnInit {

  user = JSON.parse(localStorage.getItem("user"));
  role = JSON.parse(localStorage.getItem("role"));
  records: Array<TeachingRecord> = [];
  allSubjects: Array<Subject> = [];
  allClassrooms: Array<Classroom> = [];
  allLecturers: Array<LecturerJoin> = [];
  allDepartments: Array<Department> = [];
  allDepartmentNames: Array<String> = [];
  isLoaded: boolean = false;
  isSorted: boolean = false;
  isDesc: boolean = true;
  currentElement;
  ordinal  = [];
  page = 1;
  pageSize = 10;
  closeResult = '';
  recordId: Number;

  maintainedClasses: Number = 0;
  notMaintainedClasses: Number = 0;

  order: string = 'element';
  reverse: boolean = false;
  sortedRecords: Array<TeachingRecord> = [];
  
  searchLecturer: String = '';
  searchSubject: String = '';
  searchClassroom: String = '';
  searchNumber: String = '';
  searchType: String = '';
  searchDepartment: String = '';

  allNumberOfClasses = [
    {
      id: 1,
      number: "Neodržani časovi",
      value: "0"
    },
    {
      id: 2,
      number: "1",
      value: "1"
    },
    {
      id: 3,
      number: "2",
      value: "2"
    },
    {
      id: 4,
      number: "3",
      value: "3"
    }
  ];

  allTypesOfMaintaining = [
    {
      id: 1,
      type: "Predavanja",
      value: "Predavanja"
    },
    {
      id: 2,
      type: "Vezbe",
      value: "Vezbe"
    }
  ];

  constructor(private service: RecordsService, private orderPipe: OrderPipe,
     public userService: UserService, private modalService: NgbModal, private toastr: ToastrService, private router: Router) { 
    this.sortedRecords = orderPipe.transform(this.records, 'element');
  }

  ngOnInit(): void {
    if(this.userService.roleMatch('admin')) {
      this.allRecords();
    }
    else {
      this.allRecordsById();
    }
    this.getSubjects();
    this.getClassrooms();
    this.getLecturers();
    this.getDepartments();
  }
  
  allRecords() {
    this.service.getAllRecords().subscribe(
      (res: any) => {
        if(res == null) {
          this.records = [];
        }
        else {
          
          this.records = res;

          for(let i=1; i<=this.records.length; i++) {
            this.ordinal.push(i);
          }

          let counter = 0;
          res.forEach(element => {
            if(element.maintained) {
              this.maintainedClasses += element.numberOfClasses;
            }
            else {
              counter++;
            }
          });
          this.notMaintainedClasses = counter;
        }
        this.isLoaded = true;
      },
      err => {
        console.log(err);
      }
    )
  }

  allRecordsById(){
    let user = JSON.parse(localStorage.getItem("user"));
    this.service.getRecords(user).subscribe(
      (res: any) => {
        this.isLoaded = true;
        this.records = res;

        for(let i=1; i<=this.records.length; i++) {
          this.ordinal.push(i);
        }

        let counter = 0;
        res.forEach(element => {
          if(element.maintained) {
            this.maintainedClasses += element.numberOfClasses;
          }
          else {
            counter++;
          }
        });
        this.notMaintainedClasses = counter;


      },
      err => {
        console.log(err);
      }
    )
  }

  showNumber(array, i) {
    return array[i];
  }

  pageChanged(event: PageChangedEvent): void {
    this.ordinal = [];
    let eventPage = Number(event);
    let counter = (eventPage - 1)  * this.pageSize;
    for(let i = counter; i<=this.records.length; i++){
      this.ordinal.push(i+1);
    }
  }

  maintained(value) {
    switch (value) {
      case true:
        value = "✔";
        break;
      case false:
      value = "✖";
      break;
      }
      return value;
  }

  setOrder(value: string, $event) {
    $("i").remove();
    let HTMLelement = $event.target;
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    if(this.reverse) {
      $(HTMLelement).prepend("<i class='fa fa-arrow-down' aria-hidden='true'></i>");
    }
    else {
      $(HTMLelement).prepend("<i class='fa fa-arrow-up' aria-hidden='true'></i>");
    }

    $("i").each(function (){
      this.style.pointerEvents = 'none'; 
    }); 

    this.order = value;

    //this.page = 1;
  }

  getSubjects() {
    this.service.getSubjects().subscribe(
      (res: any) => {
        this.allSubjects = res;
      },
      err => {
        console.log(err);
      }
    )
  }

  getClassrooms() {
    this.service.getClassrooms().subscribe(
      (res: any) => {
        this.allClassrooms = res;
      },
      err => {
        console.log(err);
      }
    )
  }

  getLecturers() {
    this.service.getLecturersJoin().subscribe(
      (res: any) => {
        this.allLecturers = res;
      },
      err => {
        console.log(err);
      }
    )
  }

  getDepartments() {
    this.service.getDepartments().subscribe(
      (res: any) => {
        this.allDepartments = res;
        res.forEach(element => {
          this.allDepartmentNames.push(element.name);
        });
      },
      err => {
        console.log(err);
      }
    )
  }

  deleteRecordById() {
    this.service.delete(this.recordId).subscribe(
      (res: any) => {
        this.toastr.success("Uspešno ste obrisali evidenciju!");
        this.router.navigateByUrl('/dashboard/records');
        if(this.userService.roleMatch('admin')) {
          this.allRecords();
        }
        else {
          this.allRecordsById();
        }
        this.recordId = null;
      },
      err => {
        this.toastr.error("Neuspešno brisanje evidencije!");
        console.log(err);
      }
    )
    
  }

  open(content, value) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

    this.recordId = value;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  showResetFilter() {
    if( this.searchLecturer != '' ||
        this.searchSubject != '' ||
        this.searchClassroom != '' ||
        this.searchNumber != '' ||
        this.searchType != '' ||
        this.searchDepartment != ''
      ) {
        return false;
      }
    else {
      return true;
    }
  }

  resetFilter() {
    this.searchLecturer = '';
    this.searchSubject = '';
    this.searchClassroom = '';
    this.searchNumber = '';
    this.searchType = '';
    this.searchDepartment = '';
  }

  clearDropdownDepartment() {
    this.searchDepartment = '';
  }

  clearDropdownSubject() {
    this.searchSubject = '';
  }

  clearDropdownClassroom() {
    this.searchClassroom = '';
  }

  clearDropdownLecturer() {
    this.searchLecturer = '';
  }

  clearDropdownNumberOfClasses() {
    this.searchNumber = '';
  }

  clearDropdownType() {
    this.searchType = '';
  }

  

}
