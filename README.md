# SSA2020-Front-evidencija-odrzane-nastave

U cilju kreiranja mesečnih izveštaja o održanoj nastavi pri katedrama, potrebno je uraditi evidenciju održane nastave. 
Na nivou meseca se kreiraju izveštaji za svaku katedru o održanoj nastavi.